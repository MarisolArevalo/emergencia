/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionasistencias;

import excepciones.ElementoExistenteExcepcion;
import excepciones.ElementoNoEncontradoExcepcion;
import gestionempleados.Chofer;
import gestionempleados.Doctor;
import gestionempleados.Empleado;
import gestionempleados.Enfermero;
import java.util.List;
import java.util.ArrayList;


/**
 *
 * @author 54383
 */
public class RepositorioAsistencia {

    private List<Solicitudasistencia> solicitudes;
    
    
    public RepositorioAsistencia() {
        this.solicitudes = new ArrayList<Solicitudasistencia>();
    }
   
    //Métodos 
    
      // Agregar una solicitud a la lista de solicitudes
    /*public void agregarSolicitud(Solicitudasistencia asistencia,List<Empleado> empleadosAsignados ) throws ElementoExistenteExcepcion,ElementoNoEncontradoExcepcion {
        Solicitudasistencia busqueda=this.buscarSolicitudasistencia(asistencia);
        if(busqueda==null){
	    asistencia.setEmpleadosAsignados(empleadosAsignados);
            solicitudes.add(asistencia);
        }else if (busqueda.equals(asistencia)){
            throw new ElementoExistenteExcepcion ("La Solicitud ya se encuentra esta en la lista");
        } 
	
    }*/
    public void agregarSolicitud(Solicitudasistencia asistencia) throws ElementoExistenteExcepcion {
	for (Solicitudasistencia solicitud : solicitudes) {
	    if (solicitud.getAfSolicitante().equals(asistencia.getAfSolicitante()) &&
		solicitud.getFechaSolicitud().equals(asistencia.getFechaSolicitud())) {
		throw new ElementoExistenteExcepcion("La solicitud ya existe para este afiliado en la misma fecha.");
	    }
	}
	solicitudes.add(asistencia);
    }
    
    // Buscar una solicitud por número de seguro social
    private Solicitudasistencia buscarSolicitudasistencia(Solicitudasistencia buscar) throws ElementoNoEncontradoExcepcion {
        if(buscar!=null){
             for (Solicitudasistencia solicitud : solicitudes) {
            if (buscar.equals(solicitud)) {
                return solicitud;
            }
         }
      }else if(buscar==null){
         throw new ElementoNoEncontradoExcepcion("No se encontro la Solicitud que buscaba en la lista");
         }   
        return null;
    }  
    
    





    // Eliminar una solicitud por número de seguro social
    /*public void eliminarSolicitud(Solicitudasistencia eliminarSolicitud) throws ElementoNoEncontradoExcepcion {
        Solicitudasistencia buscar=this.buscarSolicitudasistencia(eliminarSolicitud);
        if(buscar==null){
            throw new ElementoNoEncontradoExcepcion ("La solicitud que desea eliminar es inexistente");
        }else{
           System.out.println("Se elimino la solicitud del paciente" + eliminarSolicitud.getAfSolicitante());
           solicitudes.remove(buscar);   
        }
    }*/
    
    //Modificar una solicitud por númeero de seguro social
       public void modificarSolicitudasistencia (Solicitudasistencia verdadera, Solicitudasistencia modificada)throws ElementoExistenteExcepcion, ElementoNoEncontradoExcepcion{
        
       Solicitudasistencia buscar=buscarSolicitudasistencia(verdadera);
       if(buscar==null){
           throw new ElementoNoEncontradoExcepcion("La solicitud que usted desea modificar es inexistente");
       } else{
           int modificar=solicitudes.indexOf(buscar);
           solicitudes.set(modificar,modificada);
           System.out.println("Se ha modificado con exito la solicitud" + modificada.getAfSolicitante());
       }
       
     }
}
