/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionasistencias;


import gestionafiliado.Afiliado;
import gestionafiliado.Domicilio;
import gestionempleados.Chofer;
import gestionempleados.Doctor;
import gestionempleados.Empleado;
import gestionempleados.Enfermero;
import gestionmoviles.Movil;
import java.util.Objects;
import java.util.List;
import java.time.LocalDate;

        
        
/**
 *
 * @author flowe
 */
public class Solicitudasistencia{
    
    private Afiliado afSolicitante;
    private LocalDate fechaDeSolicitud;
    private Doctor doctor;
    private Enfermero enfermero;
    private Chofer chofer;
    private Movil movilAsignado;
    private String tipoAtencionRecibida;
    Domicilio domicilio;

    public Solicitudasistencia(Afiliado afSolicitante, LocalDate fechaDeSolicitud, Doctor doctor, Enfermero enfermero, Chofer chofer, Movil movilAsignado, String tipoAtencionRecibida, Domicilio domicilio) {
	this.afSolicitante = afSolicitante;
	this.fechaDeSolicitud = fechaDeSolicitud;
	this.doctor = doctor;
	this.enfermero = enfermero;
	this.chofer = chofer;
	this.movilAsignado = movilAsignado;
	this.tipoAtencionRecibida = tipoAtencionRecibida;
	this.domicilio = domicilio;
    }

    public LocalDate getFechaDeSolicitud() {
	return fechaDeSolicitud;
    }

    public Doctor getDoctor() {
	return doctor;
    }

    public Enfermero getEnfermero() {
	return enfermero;
    }

    public Chofer getChofer() {
	return chofer;
    }

    public Domicilio getDomicilio() {
	return domicilio;
    }

    
    
    public Afiliado getAfSolicitante() {
        return afSolicitante;
    }

    public void setAfSolicitante(Afiliado afSolicitante) {
        this.afSolicitante = afSolicitante;
    }

    public LocalDate getFechaSolicitud() {
        return fechaDeSolicitud;
    }

    public void setFechaSolicitud(LocalDate fechaSolicitud) {
        this.fechaDeSolicitud = fechaSolicitud;
    }

    public Movil getMovilAsignado() {
        return movilAsignado;
    }

    public void setMovilAsignado(Movil movilAsignado) {
        this.movilAsignado = movilAsignado;
    }


    public String getTipoAtencionRecibida() {
        return tipoAtencionRecibida;
    }
    
    public void setTipoAtencionRecibida(String tipoAtencionRecibida) {    
        this.tipoAtencionRecibida = tipoAtencionRecibida;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.afSolicitante);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Solicitudasistencia other = (Solicitudasistencia) obj;
        return Objects.equals(this.afSolicitante, other.afSolicitante);
    }

}
