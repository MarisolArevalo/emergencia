/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionmoviles;

import excepciones.ElementoExistenteExcepcion;
import excepciones.ElementoNoEncontradoExcepcion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class RepositorioMoviles {
     private List <Movil> moviles;

    //Constructor
    public RepositorioMoviles() {
        this.moviles = new ArrayList <Movil>();
    }
    
      public List<Movil> getMoviles(){
        return moviles;
    }
    
      
    public void agregarMovil (Movil nuevoMovil) throws ElementoExistenteExcepcion{
        if(moviles.contains(nuevoMovil)){
            throw new ElementoExistenteExcepcion ("El Movil ya esta en la lista"); 
        }
	else moviles.add(nuevoMovil);
    }
    
    
    public Movil buscarMovil(String patente) throws ElementoNoEncontradoExcepcion{
	for (Movil movil : moviles){
	    if (movil.getPatente().equals(patente)){
		 return movil;
	    }
	}      
        throw new ElementoNoEncontradoExcepcion ("No se encontro el movil en la lista");
    }
    
    
    public void eliminarMovil (String patente) throws ElementoNoEncontradoExcepcion{
            Movil movilAEliminar = buscarMovil(patente);
	    moviles.remove(movilAEliminar);
    }

        
    
    /*public void modificarMovil (Movil originalMovil, Movil nuevoMovil)throws ElementoExistenteExcepcion, ElementoNoEncontradoExcepcion{
         Movil buscar=buscarMovil(originalMovil);
        if(buscar == null){
           throw new ElementoNoEncontradoExcepcion ("El movil que desea modificar no se encuentra en la lista");
        }else {
            int busqueda=moviles.indexOf(buscar);
            moviles.set(busqueda, nuevoMovil);
            System.out.println("Se modifico exitosamente el vehiculo con patente: " + nuevoMovil.getPatente());
      }
   }*/
}