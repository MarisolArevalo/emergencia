/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionmoviles;


import java.util.Objects;

/**
 *
 * @author Usuario
 */
public class Movil {
    //atributos
    String patente;
    String marca;
    String modelo;
    String estado;

    public Movil(String patente, String marca, String modelo, String estado) {
        this.patente = patente;
        this.marca = marca;
        this.modelo = modelo;
        this.estado = estado;
    }


    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    

   
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override //sobrescribir un metodo
    
    public boolean equals(Object obj) {
        if (this == obj) { //comparamos si el objeto existe
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            //verificamos si el objeto es nulo o si pertenece a una clase diferente(getClass)
            return false;
        }
        //Si no se cumplen las opciones anteriores
        final Movil other = (Movil) obj; //Casting para convertir a "obj" en un objeto Movil
        return Objects.equals(this.patente, other.patente);
        //comparamos los atributos
    }

}
