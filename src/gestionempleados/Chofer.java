/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionempleados;

import gestionafiliado.Domicilio;
import java.time.LocalDate;

/**
 *
 * @author Ariana
 */
public class Chofer extends Empleado {
    

    public Chofer (String numeroDni, String apellido, String nombre, String telefono, LocalDate fechaNacimiento, Domicilio domicilio, String especialidad) {
	super(numeroDni, apellido, nombre, telefono, fechaNacimiento, domicilio, especialidad);
    }

}
