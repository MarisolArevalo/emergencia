/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionempleados;

import excepciones.ElementoExistenteExcepcion;
import excepciones.ElementoNoEncontradoExcepcion;
import java.util.*;
/**
 *
 * @author flowe
 */

public class RepositorioEmpleados {
   private List <Empleado> empleados; 

    
    public RepositorioEmpleados(){
        this.empleados = new ArrayList<Empleado>();
    }
    
    public List<Empleado> getEmpleados(){
        return empleados;
    }
   
    public void agregarEmpleado(Empleado empleado)throws ElementoExistenteExcepcion {
        if (empleados.contains(empleado)) {
            throw new ElementoExistenteExcepcion("El empleado ya existe");
        } 
        else empleados.add(empleado);
    }
    
    
     public List<Empleado> mostrarListaDeEmpleados (){
        return empleados;
    }
     
     
  
     public Empleado buscarEmpleado(String numDni)throws ElementoNoEncontradoExcepcion {
        for (Empleado empleado : empleados) {
            if (empleado.getNumeroDni().equals(numDni)) {
                return empleado;
            }
        }
       throw new ElementoNoEncontradoExcepcion("No se encontro al empleado");
    }
     
     public Doctor buscarDoctorPorNombre(String nombreDoctor) throws ElementoNoEncontradoExcepcion {
        for (Empleado empleado : empleados) {
            if ("doctor".equalsIgnoreCase(empleado.getCategoria()) && empleado.getNombre().equalsIgnoreCase(nombreDoctor)) {
                return (Doctor) empleado;
            }
        }
        throw new ElementoNoEncontradoExcepcion("No se encontró al doctor con nombre: " + nombreDoctor);

     }
     
	public Chofer buscarChoferPorNombre(String nombreChofer) throws ElementoNoEncontradoExcepcion {
        for (Empleado empleado : empleados) {
            if ("chofer".equalsIgnoreCase(empleado.getCategoria()) && empleado.getNombre().equalsIgnoreCase(nombreChofer)) {
                return (Chofer) empleado;
            }
        }
        throw new ElementoNoEncontradoExcepcion("No se encontró al chofer con nombre: " + nombreChofer);
    }

    public Enfermero buscarEnfermeroPorNombre(String nombreEnfermero) throws ElementoNoEncontradoExcepcion {
        for (Empleado empleado : empleados) {
            if ("enfermero".equalsIgnoreCase(empleado.getCategoria()) && empleado.getNombre().equalsIgnoreCase(nombreEnfermero)) {
                return (Enfermero) empleado;
            }
        }
        throw new ElementoNoEncontradoExcepcion("No se encontró al enfermero con nombre: " + nombreEnfermero);
    }
     
     
      public void eliminarEmpleado(String numDni) throws ElementoNoEncontradoExcepcion {
        Empleado empleadoAEliminar = buscarEmpleado(numDni);
            empleados.remove(empleadoAEliminar);
    }
      
      
     public void modificarEmpleado (String dni, Empleado nuevoEmpleado)throws ElementoNoEncontradoExcepcion {
        Empleado empleadoExistente = buscarEmpleado(dni);

        // Elimina el afiliado existente con el DNI proporcionado
        eliminarEmpleado(dni);
        // Luego, agrega el nuevo afiliado con los datos actualizados
        empleados.add(nuevoEmpleado);
    }
     
 
}
