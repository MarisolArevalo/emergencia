/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionempleados;

import gestionafiliado.Domicilio;
import gestionafiliado.Persona;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author flowe
 */
public class Empleado extends Persona {
   
    private String categoria;

    public Empleado(String numeroDni, String apellido, String nombre, String telefono, LocalDate fechaNacimiento, Domicilio domicilio, String categoria) {
        super(numeroDni, apellido, nombre, telefono, fechaNacimiento, domicilio);
        this.categoria = categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    
    public String getCategoria() {
        return categoria;
    }

  

    @Override
   public boolean equals(Object o) {
    if (this == o) {
        return true;
    }
    if (o == null || getClass() != o.getClass()) {
        return false;
    }
    Empleado empleado = (Empleado) o;
     return empleado.getNumeroDni().equals(empleado.getNumeroDni()) && Objects.equals(categoria, empleado.categoria);

   }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.categoria);
        return hash;
    }

  
    
    
    
   
}