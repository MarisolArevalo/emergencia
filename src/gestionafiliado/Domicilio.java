/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliado;

import java.time.LocalDate;

/**
 *
 * @author 54383
 */
public class Domicilio {
    private String calle;
    private String altura;

    public Domicilio(String calle, String altura) {
        this.calle = calle;
        this.altura = altura;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }
    

    
    public String getCalle() {
        return calle;
    }


    public String getAltura() {
        return altura;
    }
    
}
