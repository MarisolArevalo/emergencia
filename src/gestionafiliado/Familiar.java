/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliado;

import java.time.LocalDate;

/**
 *
 * @author 54383
 */
public class Familiar  {
    private String parentesco;
    private Afiliado afiliado;

    //Volver a agregar fecha de nacimiento
    /*public Familiar(String numeroDni, String apellido, String nombre, String telefono, LocalDate fechaDeNacimiento, Domicilio domicilio, String parentesco) {
        super(numeroDni, apellido, nombre, telefono,fechaDeNacimiento, domicilio);
        this.parentesco = parentesco;
    }
    public Familiar(Afiliado afiliado, String parentesco) {
        super(afiliado.getNumeroDni(), afiliado.getApellido(), afiliado.getNombre(), afiliado.getTelefono(), afiliado.getFechaDeNacimiento(), afiliado.getDomicilio());
        this.parentesco = parentesco;
        this.afiliado = afiliado;
    }*/
    
    public Familiar(Afiliado afiliado, String parentesco){
	this.afiliado = afiliado;
	this.parentesco = parentesco;
    }

    public void setParentesco(String parentesco) {
	this.parentesco = parentesco;
    }

    public String getDni (){
	return afiliado.getNumeroDni();
    }

    public String getNombre (){
	return afiliado.getNombre();
    }

    
    public String getParentesco() {
        return parentesco;
    }

    
}
