/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliado;


import excepciones.ElementoExistenteExcepcion;
import excepciones.ElementoNoEncontradoExcepcion;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 54383
 */
public class Afiliado extends Persona {
    private int codigoAfiliado;
    private static int codigoSiguiente=1;
    private LocalDate fechaDeAfiliacion;
    private List <Familiar> grupoFamiliar;
    private String parentesco;
    

    public Afiliado(String numeroDni, String apellido, String nombre, String telefono,LocalDate fechaNacimiento,LocalDate fechaAfiliacion, Domicilio domicilio) {
        super(numeroDni, apellido, nombre, telefono, fechaNacimiento, domicilio);
        this.grupoFamiliar = new ArrayList<Familiar>();
	this.fechaDeAfiliacion=fechaAfiliacion;
	//this.parentesco= parentesco;
        codigoAfiliado=codigoSiguiente;
        codigoSiguiente++;
    }

    public void setCodigoAfiliado(int codigoAfiliado) {
        this.codigoAfiliado = codigoAfiliado;
    }
 
    
    public int getCodigoAfiliado() {
        return codigoAfiliado;
    }

    public LocalDate getFechaDeAfiliacion() {
	return fechaDeAfiliacion;
    }
    
    

    public void setFechaDeAfiliacion(LocalDate fechaDeAfiliacion) {
	this.fechaDeAfiliacion = fechaDeAfiliacion;
    }
    
    
    /*public boolean esTitular(){
	return "Titular".equals(parentesco);
    }*/
    public boolean esTitular() {
        // Verifica si el afiliado tiene al menos un familiar con el parentesco "Titular"
        for (Familiar familiar : grupoFamiliar) {
            if ("Titular".equals(familiar.getParentesco())) {
                return true;
            }
        }
        return false;
    }

    

    public void agregarFamiliar(Familiar familiar) throws ElementoExistenteExcepcion{
        if (grupoFamiliar.contains(familiar)){
            throw new ElementoExistenteExcepcion("El familiar ya esta en la lista");
        } else
        grupoFamiliar.add(familiar);
    }
    
    
    public Familiar buscarFamiliar (String dni) throws ElementoNoEncontradoExcepcion{
        for (Familiar familiar : grupoFamiliar){
            if (familiar.getDni().equals(dni)){
                return familiar;
            }
        }
        throw new ElementoNoEncontradoExcepcion("El familiar no se encontró en la lista");
    }
    
    public void eliminarFamiliar (String dni) throws ElementoNoEncontradoExcepcion{
        Familiar familiar = buscarFamiliar(dni);
            grupoFamiliar.remove(familiar);
    }
    
        
    public List<Familiar> mostrarFamiliares(){
        return grupoFamiliar;
    }
    
}
