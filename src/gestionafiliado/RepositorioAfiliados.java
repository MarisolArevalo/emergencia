/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliado;

import excepciones.ElementoExistenteExcepcion;
import excepciones.ElementoNoEncontradoExcepcion;
import java.util.*;

/**
 *
 * @author Usuario
 */
public class RepositorioAfiliados{
    List <Afiliado> afiliados;

    
    
    public RepositorioAfiliados() {
        this.afiliados = new ArrayList<Afiliado>();
    }

    
    public void agregarAfiliado (Afiliado nuevoAfiliado)throws ElementoExistenteExcepcion{
        if (afiliados.contains(nuevoAfiliado)){
            throw new ElementoExistenteExcepcion("El afiliado ya esta en la lista");
        } else
        afiliados.add(nuevoAfiliado);
       //Se agrega un afiliado a la lista
    }
    

    public Afiliado buscarAfiliado (String dni) throws ElementoNoEncontradoExcepcion{   
        for (Afiliado afiliado : afiliados){
            if (afiliado.getNumeroDni().equals(dni)){
                return afiliado;
            }
        }
        throw new ElementoNoEncontradoExcepcion("El afiliado no se encontró en la lista");
    }
    
 
    
    /*public boolean esTitular (String dni){
	for (Afiliado afiliado : afiliados){
	    if (afiliado.getNumeroDni().equals(dni) && afiliado.esTitular()){
		return true;
	    }
	}
	return false;
    }*/
    
    
    public void eliminarAfiliado (String dni) throws ElementoNoEncontradoExcepcion{
        Afiliado afiliado = buscarAfiliado(dni);
            afiliados.remove(afiliado);
    }
    
    
    
    public List<Afiliado> mostrarListaDeAfiliados (){
        return afiliados;
    }
  
    public void modificarAfiliado(String dni, Afiliado nuevoAfiliado) throws ElementoNoEncontradoExcepcion {
        eliminarAfiliado(dni);
        afiliados.add(nuevoAfiliado);
    }


}
