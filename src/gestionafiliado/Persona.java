/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliado;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author 54383
 */
public class Persona {
    private String numeroDni;
    private String apellido;
    private String nombre;
    private String telefono;
    private LocalDate fechaDeNacimiento;
    private Domicilio domicilio; 

    public Persona(String numeroDni, String apellido, String nombre, String telefono, LocalDate fechaNacimiento, Domicilio domicilio) {
        this.numeroDni = numeroDni;
        this.apellido = apellido;
        this.nombre = nombre;
        this.telefono = telefono;
        this.fechaDeNacimiento = fechaNacimiento;
        this.domicilio = domicilio;
    }


    public Domicilio getDomicilio() {
        return domicilio;
    }

    public String getNumeroDni() {
        return numeroDni;
    }

    public String getApellido() {
        return apellido;
    }

    public String getNombre() {
        return nombre + " " + apellido;
    }
    

    public String getTelefono() {
        return telefono;
    }

    public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
	this.fechaDeNacimiento = fechaDeNacimiento;
    }

    
    public LocalDate getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.numeroDni);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        return Objects.equals(this.numeroDni, other.numeroDni);
    }

    public void setNumeroDni(String numeroDni) {
        this.numeroDni = numeroDni;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }


    
}
