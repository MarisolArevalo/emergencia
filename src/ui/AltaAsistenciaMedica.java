/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package ui;

/**
 *
 * @author 54383
 */
import emergencia.Emergencia;
import emergencia.Empresa;
import excepciones.ElementoExistenteExcepcion;
import excepciones.ElementoNoEncontradoExcepcion;
import gestionafiliado.Afiliado;
import gestionafiliado.Domicilio;
import gestionafiliado.RepositorioAfiliados;
import gestionasistencias.RepositorioAsistencia;
import gestionasistencias.Solicitudasistencia;
import gestionempleados.Chofer;
import gestionempleados.Doctor;

import gestionempleados.Empleado;
import gestionempleados.Enfermero;
import gestionempleados.RepositorioEmpleados;
import gestionmoviles.Movil;
import gestionmoviles.RepositorioMoviles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;


public class AltaAsistenciaMedica extends javax.swing.JFrame {
        
        private final RepositorioMoviles repositorioMoviles = Empresa.instancia().getRepositorioMoviles();
	private final RepositorioAfiliados repositorioAfiliados = Empresa.instancia().getRepositorioAfiliados();
	private final RepositorioEmpleados repositorioEmpleados = Empresa.instancia().getRepositorioEmpleado();
	private final RepositorioAsistencia repositorioAsistencia = Empresa.instancia().getRepositorioAsistencia();
	private String dniAfiliado;
        
        public AltaAsistenciaMedica(String dniAfiliado){
            initComponents();
	    this.dniAfiliado = dniAfiliado;
	    nombreAfiliado.setText(dniAfiliado);
	    llenarComboMovil();
	    
	    
	    // Llenar el comboChofer con los empleados que son choferes
	    List<Empleado> choferes = obtenerChoferes();
	    DefaultComboBoxModel<String> modelChofer = new DefaultComboBoxModel<>();
	    for (Empleado chofer : choferes) {
		modelChofer.addElement(chofer.getNombre()); // Agrega el nombre del chofer al combo
	    }
	    comboChofer.setModel(modelChofer);
	    
	    // Llenar el comboDoctores con los empleados que son doctores
	    List<Empleado> doctores = obtenerDoctores();
	    DefaultComboBoxModel<String> modelDoctores = new DefaultComboBoxModel<>();
	    for (Empleado doctor : doctores) {
		modelDoctores.addElement(doctor.getNombre()); // Agrega el nombre del doctor al combo
	    }
	    comboDoctores.setModel(modelDoctores);
	    
	    List<Empleado> enfermeros = obtenerEnfermeros();
	    DefaultComboBoxModel<String> modelEnfermero = new DefaultComboBoxModel<>();
	    for (Empleado enfermero : enfermeros) {
		modelEnfermero.addElement(enfermero.getNombre()); // Agrega el nombre del enfermero al combo
	    }
	    comboEnfermero.setModel(modelEnfermero);
	    
        }
	
	private List<Empleado> obtenerChoferes() {
	    List<Empleado> empleados = repositorioEmpleados.getEmpleados();
	    List<Empleado> choferes = new ArrayList<>();

	    for (Empleado empleado : empleados) {
		if ("chofer".equalsIgnoreCase(empleado.getCategoria())) {
		    choferes.add(empleado);
		}
	    }
	    return choferes;
	}
	
	private List<Empleado> obtenerDoctores() {
	    List<Empleado> empleados = repositorioEmpleados.getEmpleados();
	    List<Empleado> doctores = new ArrayList<>();

	    for (Empleado empleado : empleados) {
		if ("doctor".equalsIgnoreCase(empleado.getCategoria())) {
		    doctores.add(empleado);
		}
	    }

	    return doctores;
	}
	
	private List<Empleado> obtenerEnfermeros() {
	    List<Empleado> empleados = repositorioEmpleados.getEmpleados();
	    List<Empleado> enfermeros = new ArrayList<>();

	    for (Empleado empleado : empleados) {
		if ("enfermero".equalsIgnoreCase(empleado.getCategoria())) {
		    enfermeros.add(empleado);
		}
	    }

	    return enfermeros;
	}
	
	private void llenarComboMovil() {

        List<Movil> moviles = repositorioMoviles.getMoviles();

        comboMovil.removeAllItems();

        for (Movil movil : moviles) {
            comboMovil.addItem(movil.getPatente());
        }
    }
	
    
        
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        comboDoctores = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboEnfermero = new javax.swing.JComboBox<>();
        comboChofer = new javax.swing.JComboBox<>();
        comboMovil = new javax.swing.JComboBox<>();
        botonAgregar = new javax.swing.JButton();
        botonAtras = new javax.swing.JButton();
        fechaActual = new javax.swing.JFormattedTextField();
        tipoDeAtencion = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        textCalle = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        textAltura = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        nombreAfiliado = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 255, 204));

        jPanel1.setBackground(new java.awt.Color(233, 250, 233));
        jPanel1.setForeground(new java.awt.Color(51, 255, 255));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel2.setText("Fecha:");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel3.setText("Tipo de Asistencia:");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel4.setText("Doctor:");

        comboDoctores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboDoctoresActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel5.setText("Chofer:");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel6.setText("Movil:");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel7.setText("Enfermero:");

        botonAgregar.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        botonAgregar.setText("Agregar Solicitud");
        botonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarActionPerformed(evt);
            }
        });

        botonAtras.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        botonAtras.setText("Atras");
        botonAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAtrasActionPerformed(evt);
            }
        });

        try {
            fechaActual.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        tipoDeAtencion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Atencion Primaria", "Atencion Hospitalaria", "Atencion de Urgencias" }));

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel9.setText("Domicilio");

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel10.setText("Calle");

        textCalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textCalleActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel11.setText("Altura");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboDoctores, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboEnfermero, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboChofer, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboMovil, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(154, 154, 154))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(botonAgregar)
                                .addGap(358, 358, 358)
                                .addComponent(botonAtras))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fechaActual, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(161, 161, 161)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tipoDeAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 74, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(textCalle, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(35, 35, 35)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(tipoDeAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechaActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textCalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(textAltura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(53, 53, 53)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(comboDoctores, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(comboEnfermero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(comboChofer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(comboMovil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonAgregar)
                    .addComponent(botonAtras))
                .addGap(22, 22, 22))
        );

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel1.setText("Solicitud de Asistencia Medica");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel8.setText("DNI del Afiliado");

        nombreAfiliado.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        nombreAfiliado.setText("jLabel12");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 573, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(176, 176, 176)
                .addComponent(jLabel8)
                .addGap(34, 34, 34)
                .addComponent(nombreAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(nombreAfiliado))
                .addGap(0, 33, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboDoctoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboDoctoresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboDoctoresActionPerformed

    private void botonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarActionPerformed
        // TODO add your handling code here:
	
	try {
	    String dniDelAfiliado = this.dniAfiliado;
	    String fecha = fechaActual.getText();
	    String tipoAtencion = (String) tipoDeAtencion.getSelectedItem();
	    String nombreDoctor = (String) comboDoctores.getSelectedItem();
	    String nombreEnfermero = (String) comboEnfermero.getSelectedItem();
	    String nombreChofer = (String) comboChofer.getSelectedItem();
	    String patenteMovil = (String) comboMovil.getSelectedItem();
	    String calle = textCalle.getText();
	    String altura = textAltura.getText();

	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	    LocalDate fechaSolicitud = LocalDate.parse(fecha, formatter);

	    Afiliado afiliado = repositorioAfiliados.buscarAfiliado(dniDelAfiliado);
	    Domicilio domicilio = new Domicilio (calle, altura);
	    
	    Doctor doctor =(Doctor) repositorioEmpleados.buscarDoctorPorNombre(nombreDoctor);
	    Enfermero enfermero =(Enfermero) repositorioEmpleados.buscarEnfermeroPorNombre(nombreEnfermero);
	    Chofer chofer =(Chofer) repositorioEmpleados.buscarChoferPorNombre(nombreChofer);
	    Movil movil = repositorioMoviles.buscarMovil(patenteMovil);
	
	    Solicitudasistencia solicitud = new Solicitudasistencia (afiliado, fechaSolicitud, doctor, enfermero, chofer, movil, tipoAtencion, domicilio);
	    repositorioAsistencia.agregarSolicitud(solicitud);
	    JOptionPane.showMessageDialog(rootPane, "Solicitud de asistencia agregada con éxito", "Éxito", JOptionPane.INFORMATION_MESSAGE);
	    } catch (ElementoExistenteExcepcion e) {
		JOptionPane.showMessageDialog(rootPane, "Ya se registro la solicitud", "Error", JOptionPane.ERROR_MESSAGE);
	    } catch (ElementoNoEncontradoExcepcion e) {
		JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    }    
           
    }//GEN-LAST:event_botonAgregarActionPerformed

    private void botonAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAtrasActionPerformed
        // TODO add your handling code here:
           VentanaPrincipal ventanaPrincipal = new VentanaPrincipal();
           ventanaPrincipal.setVisible(true);
	   ventanaPrincipal.setBounds(350,100,700,500);
           this.dispose();
    }//GEN-LAST:event_botonAtrasActionPerformed

    private void textCalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textCalleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textCalleActionPerformed
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregar;
    private javax.swing.JButton botonAtras;
    private javax.swing.JComboBox<String> comboChofer;
    private javax.swing.JComboBox<String> comboDoctores;
    private javax.swing.JComboBox<String> comboEnfermero;
    private javax.swing.JComboBox<String> comboMovil;
    private javax.swing.JFormattedTextField fechaActual;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel nombreAfiliado;
    private javax.swing.JTextField textAltura;
    private javax.swing.JTextField textCalle;
    private javax.swing.JComboBox<String> tipoDeAtencion;
    // End of variables declaration//GEN-END:variables
}
