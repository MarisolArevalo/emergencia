/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionpagos;

import gestionafiliado.Afiliado;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Usuario
 */

//En esta clase vamos a representar la factura emitida
public class Factura {
    private String numeroFactura;
    private Afiliado paciente;
    private LocalDate fechaEmision;
    public List<Item> items;

    public Factura(String numeroFactura, Afiliado paciente, LocalDate fechaEmision) {
        this.numeroFactura = numeroFactura;
        this.paciente = paciente;
        this.fechaEmision = fechaEmision;
    }

    

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public Afiliado getPaciente() {
        return paciente;
    }
    public LocalDate getFechaEmision() {
        return fechaEmision;
    }

    public List<Item> getItems() {
        return items;
    }
    
    
   
    
        
    
    //La clase interna la vamos a usar para mostrar los datos de la factura
    public class Item {
       private Double tarifaBasica;
       private Double tarifaFamiliar;
       private float iva = 0.21f;
       private int cantidadFamiliares;

      //Metodo TotalItem()      

        public Item(Double tarifaBasica, Double tarifaFamiliar, float iva, int cantidadFamiliares) {
            this.tarifaBasica = tarifaBasica;
            this.tarifaFamiliar = tarifaFamiliar;
            this.iva = iva;
            this.cantidadFamiliares = cantidadFamiliares;
        }

        public double getTarifaBasica() {
            return tarifaBasica;
        }

        public void setTarifaBasica(Double tarifaBasica) {
            this.tarifaBasica = tarifaBasica;
        }

        public double getTarifaFamiliar() {
            return tarifaFamiliar;
        }

        public void setTarifaFamiliar(Double tarifaFamiliar) {
            this.tarifaFamiliar = tarifaFamiliar;
        }

        public float getIva() {
            return iva;
        }

        public void setIva(float iva) {
            this.iva = iva;
        }

        public int getCantidadFamiliares() {
            return cantidadFamiliares;
        }

        public void setCantidadFamiliares(int cantidadFamiliares) {
            this.cantidadFamiliares = cantidadFamiliares;
        }
        
         public Double totalItems(){
    
        Double totalFamiliar = cantidadFamiliares * tarifaFamiliar; 
        Double total = (tarifaBasica + totalFamiliar) - ((tarifaBasica + totalFamiliar) * iva);
        
        return total;
        
        }    
    }
    
    
     public Double totalFactura(){
        Double total=0.0;
        for(Item item : items){
            total= total+item.totalItems();
        }
        return total;
    }

   
//Metodo TotalFactura()
    
    @Override
    public boolean equals(Object o) {
    if (this == o) {
        return true; // Son el mismo objeto
    }
    if (o == null || getClass() != o.getClass()) {
        return false; // Las clases son diferentes
    }

    Factura factura = (Factura) o;
    if (numeroFactura != null) {
        return numeroFactura.equals(factura.numeroFactura);
    } else {
        return factura.numeroFactura == null;
    }
}

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.numeroFactura);
        return hash;
    }
    
    
}
