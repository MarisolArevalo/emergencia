/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionpagos;

import java.util.*;
import excepciones.ElementoExistenteExcepcion;
import excepciones.ElementoNoEncontradoExcepcion;
/**
 *
 * @author Usuario
 */

public class LibroDePagos {
    private List <Factura> facturas;
    

    public LibroDePagos() {
        this.facturas = new ArrayList<Factura>();
    }

    public List<Factura> getFacturas() {
        return facturas;
    }
    
    

    public void agregarFactura (Factura factura) throws ElementoExistenteExcepcion {
        if (facturas.contains(factura)) {
            throw new  ElementoExistenteExcepcion("La factura ya existe");
        } 
        else facturas.add(factura);
    }
    
    
   public Factura buscarFactura (String numeroFactura)throws ElementoNoEncontradoExcepcion {
        for (Factura factura : facturas) {
            if (factura.getNumeroFactura().equals(numeroFactura)) {
                return factura;
            }
        }
       throw new ElementoNoEncontradoExcepcion("No se encontro la factura");
    }
   
   public void eliminarFactura (String numeroFactura) throws ElementoNoEncontradoExcepcion {
        Factura factura = buscarFactura(numeroFactura);
            facturas.remove( factura);
    }
   
   
    
}
