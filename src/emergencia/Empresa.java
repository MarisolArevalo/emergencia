/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia;

import gestionafiliado.RepositorioAfiliados;
import gestionasistencias.RepositorioAsistencia;
import gestionempleados.RepositorioEmpleados;
import gestionmoviles.Movil;
import gestionmoviles.RepositorioMoviles;
import gestionpagos.LibroDePagos;
import java.util.List;

/**
 *
 * @author Ariana
 */
public class Empresa {
    
    private static Empresa empresa;
    private RepositorioAfiliados repositorioAfiliados;
    private RepositorioAsistencia repositorioAsistencia;
    private RepositorioEmpleados repositorioEmpleado;
    private RepositorioMoviles repositorioMoviles;
    private LibroDePagos repositorioFacturas;
    
    
    public Empresa (){
        
        repositorioAfiliados = new RepositorioAfiliados();
        repositorioAsistencia = new RepositorioAsistencia();
        repositorioEmpleado = new RepositorioEmpleados();
        repositorioMoviles = new RepositorioMoviles(); 
        repositorioFacturas = new LibroDePagos();
    }
    
    public static Empresa instancia(){
        if(empresa==null){
            empresa = new Empresa();
        }
        return empresa;
    }
    

    public void setRepositorioAfiliados(RepositorioAfiliados repoAfiliados) {
        this.repositorioAfiliados = repoAfiliados;
    }

    public void setRepositorioAsistencia(RepositorioAsistencia repositorioAsistencia) {
        this.repositorioAsistencia = repositorioAsistencia;
    }

    public void setRepositorioEmpleado(RepositorioEmpleados repositorioEmpleado) {
        this.repositorioEmpleado = repositorioEmpleado;
    }

    public void setRepositorioMoviles(RepositorioMoviles repositorioMoviles) {
        this.repositorioMoviles = repositorioMoviles;
    }

    public void setRepositorioFacturas(LibroDePagos repositorioFacturas) {
        this.repositorioFacturas = repositorioFacturas;
    }

    
    public RepositorioAfiliados getRepositorioAfiliados() {
        return repositorioAfiliados;
    }

    public RepositorioAsistencia getRepositorioAsistencia() {
        return repositorioAsistencia;
    }

    public RepositorioEmpleados getRepositorioEmpleado() {
        return repositorioEmpleado;
    }

    public RepositorioMoviles getRepositorioMoviles() {
        return repositorioMoviles;
    }

    public LibroDePagos getRepoFacturas() {
        return repositorioFacturas;
    }
    
}
